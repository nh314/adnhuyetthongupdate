<?php
/**
 * The sidebar containing the main widget area
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>
<aside class="sidebar" data-sticky-container>
	<?php do_action( 'foundationpress_before_sidebar' ); ?>
	<div class="all-sidebar-widget-wrap" >
	<?php dynamic_sidebar( 'sidebar-widgets' ); ?>
	</div>
	<?php do_action( 'foundationpress_after_sidebar' ); ?>
</aside>
