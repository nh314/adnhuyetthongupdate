<?php
if ( ! function_exists( 'hthong_customize_register' ) ):
function hthong_customize_register($wp_customize){
	$settings = array(
		'noibat_1_tieude' => array(
				'setting' => array("default" => "", "transport" => "refresh"),
				'control' => array(
						"label" => __("Nổi bật 1"),
						"section" => "static_front_page",
						"settings" => "noibat_1_tieude",						
					)
			),
		'noibat_1_lienket' => array(
				'setting' => array("default" => "", "transport" => "refresh"),
				'control' => array(
						"label" => __("Liên kết nổi bật 1"),
						"section" => "static_front_page",
						"settings" => "noibat_1_lienket",						
					)
			),
		'noibat_1_noidung' => array(
				'setting' => array("default" => "", "transport" => "refresh"),
				'control' => array(
						"label" => __("Nội dung nổi bật 1"),
						"section" => "static_front_page",
						"settings" => "noibat_1_noidung",	
						'type'		=> "textarea"					
					)
			),
		
		'noibat_2_tieude' => array(
				'setting' => array("default" => "", "transport" => "refresh"),
				'control' => array(
						"label" => __("Nổi bật 1"),
						"section" => "static_front_page",
						"settings" => "noibat_2_tieude",						
					)
			),
		'noibat_2_lienket' => array(
				'setting' => array("default" => "", "transport" => "refresh"),
				'control' => array(
						"label" => __("Liên kết nổi bật 2"),
						"section" => "static_front_page",
						"settings" => "noibat_2_lienket",						
					)
			),
		'noibat_2_noidung' => array(
				'setting' => array("default" => "", "transport" => "refresh"),
				'control' => array(
						"label" => __("Nội dung nổi bật 2"),
						"section" => "static_front_page",
						"settings" => "noibat_2_noidung",	
						'type'		=> "textarea"					
					)
			),
		'noibat_3_tieude' => array(
				'setting' => array("default" => "", "transport" => "refresh"),
				'control' => array(
						"label" => __("Nổi bật 3"),
						"section" => "static_front_page",
						"settings" => "noibat_3_tieude",						
					)
			),
		'noibat_3_lienket' => array(
				'setting' => array("default" => "", "transport" => "refresh"),
				'control' => array(
						"label" => __("Liên kết nổi bật 3"),
						"section" => "static_front_page",
						"settings" => "noibat_3_lienket",						
					)
			),
		'noibat_3_noidung' => array(
				'setting' => array("default" => "", "transport" => "refresh"),
				'control' => array(
						"label" => __("Nội dung nổi bật 3"),
						"section" => "static_front_page",
						"settings" => "noibat_3_noidung",	
						'type'		=> "textarea"					
					)
			),
		'dichvu_cat' => array(
				'setting' => array("default" => "", "transport" => "refresh"),
				'control' => array(
						"label" => __("Phân loại dịch vụ"),
						"section" => "static_front_page",
						"settings" => "dichvu_cat",
						'type'		=> "dropdown-category",						
						'hide_empty' => 0
					)
			),
		'hotro_cat' => array(
				'setting' => array("default" => "", "transport" => "refresh"),
				'control' => array(
						"label" => __("Phân loại hỗ trợ"),
						"section" => "static_front_page",
						"settings" => "hotro_cat",
						'type'		=> "dropdown-category",						
						'hide_empty' => 0
					)
			),
		'phanhoi_cat' => array(
				'setting' => array("default" => "", "transport" => "refresh"),
				'control' => array(
						"label" => __("Phân loại phản hồi"),
						"section" => "static_front_page",
						"settings" => "phanhoi_cat",
						'type'		=> "dropdown-category",						
						'hide_empty' => 0
					)
			),
		'tintuc_cat' => array(
				'setting' => array("default" => "", "transport" => "refresh"),
				'control' => array(
						"label" => __("Phân loại tin tức"),
						"section" => "static_front_page",
						"settings" => "tintuc_cat",
						'type'		=> "dropdown-category",						
						'hide_empty' => 0
					)
			),
		'faq_cat' => array(
				'setting' => array("default" => "", "transport" => "refresh"),
				'control' => array(
						"label" => __("Phân loại câu hỏi thường gặp"),
						"section" => "static_front_page",
						"settings" => "faq_cat",
						'type'		=> "dropdown-category",						
						'hide_empty' => 0
					)
			),
		'hotline_number' => array(
				'setting' => array("default" => "", "transport" => "refresh"),
				'control' => array(
						"label" => __("Số hotline"),
						"section" => "title_tagline",
						"settings" => "hotline_number",
										
						
					)
			),
		'hotline_number2' => array(
				'setting' => array("default" => "", "transport" => "refresh"),
				'control' => array(
						"label" => __("Số hotline 2"),
						"section" => "title_tagline",
						"settings" => "hotline_number2",										
						
					)
			),
		'facebook_url' => array(
				'setting' => array("default" => "", "transport" => "refresh"),
				'control' => array(
						"label" => __("Trang Facebook"),
						"section" => "title_tagline",
						"settings" => "facebook_url",
						'type'		=> "url",
					)
			),
		'twitter_url' => array(
				'setting' => array("default" => "", "transport" => "refresh"),
				'control' => array(
						"label" => __("Trang Twitter"),
						"section" => "title_tagline",
						"settings" => "twitter_url",
						'type'		=> "url",
					)
			),
		'google_plus_url' => array(
				'setting' => array("default" => "", "transport" => "refresh"),
				'control' => array(
						"label" => __("Trang Google Plus"),
						"section" => "title_tagline",
						"settings" => "google_plus_url",
						'type'		=> "url",
					)
			),

		
	);

	foreach ($settings as $key => $value) {		
		$wp_customize->add_setting($key, $value['setting']);
		if(empty($value['control']['type'])){
			$value['control']['type'] = 'default';
		}
		switch ($value['control']['type']) {
			case 'image':				
				$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, $key,$value['control']));
				break;
			case 'dropdown-category':
				$wp_customize->add_control(new WP_Customize_Category_Control($wp_customize, $key,$value['control']));
				break;
			
			default:
				$wp_customize->add_control(new WP_Customize_Control($wp_customize, $key,$value['control']));
				break;
		}
		
	}

}
add_action( 'customize_register', 'hthong_customize_register' );
endif;

if (class_exists('WP_Customize_Control')) {
    class WP_Customize_Category_Control extends WP_Customize_Control {
        public $taxonomy = 'category';
            

        public function render_content() {
            $dropdown = wp_dropdown_categories(
                array(
                    'name'              => '_customize-dropdown-categories-' . $this->id,
                    'echo'              => 0,
                    'show_option_none'  => __( '&mdash; Select &mdash;' ),
                    'option_none_value' => '0',
                    'selected'          => $this->value(),
                    'taxonomy'			=> $this->taxonomy,
                )
            );
 
            // Hackily add in the data link parameter.
            $dropdown = str_replace( '<select', '<select ' . $this->get_link(), $dropdown );
 
            printf(
                '<label class="customize-control-select"><span class="customize-control-title">%s</span> %s</label>',
                $this->label,
                $dropdown
            );
        }
    }
}
?>