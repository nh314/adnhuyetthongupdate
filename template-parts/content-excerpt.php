<div class="post-carousel">

<?php hthong_post_thumbnail('service-thumb') ?>

<?php the_title( sprintf( '<h3 class="post-title"><a href="%s" rel="bookmark" itemprop="url"><span itemprop="itemListElement"><strong>', esc_url( get_permalink() ) ), '</strong></span></a></h3>' ); ?>

			<div class="post-excerpt">

				<div itemprop="description"><?php the_excerpt(); ?>	</div>			

			</div>

</div>