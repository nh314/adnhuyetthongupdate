<?php
	$pms = new WP_Query(array(
		'post_type' => 'payment_method',
		'nopaging' => true,
		'order' => 'ASC'
	));
?>
<section class="payment-method-row">
<div class="row large-up-6 medium-up-3 small-up-2 ">
	<?php while ($pms->have_posts()): $pms->the_post();?>
	<div class="column">
		<div class="payment-method">
			<a class="pm-thumbnail" href="<?php echo esc_url( get_permalink() ) ?>">
				<?php the_post_thumbnail( 'post-thumbnail', array( 'alt' => the_title_attribute( 'echo=0' ) ) ); ?>
			</a>
			<div class="pm-info">
				<?php the_content() ?>
			</div>
			
		</div>
		
	</div>	

	<?php endwhile; ?>
</div>
</section>