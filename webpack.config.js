const path = require('path');

const BrowserSyncPlugin = require('browser-sync-webpack-plugin');


const config = {
    mode: 'development',
    entry: './src/assets/js/main.js',
    output: {
        path: path.resolve(__dirname, "assets"),
        filename: 'javascript/update.js'
    },
    module: {
        rules: [
           {
                test: /\.scss$/,
                use: ["style-loader", "css-loader", "postcss-loader",  "sass-loader"]
            }
        ]
    },
    plugins: [
         new BrowserSyncPlugin({
            port: process.env.PORT,
            open: false,
            host: process.env.IP,
            proxy: {
                target: "http://localhost:8080",
            }
        })
    ]
}

module.exports = config;
    