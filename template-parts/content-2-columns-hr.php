<div class="large-6 columns content-2-columns-hr thumb-overlay">

<?php hthong_post_thumbnail('service-thumb') ?>

	

	<h3 class="entry-title"><a href="<?php echo esc_url( get_permalink() ) ?>" title="<?php the_title() ?>" rel="bookmark" itemprop="url"><span itemprop="itemListElement"><?php the_title() ?></span></a></h3>

	<div class="entry-content">

		<div itemprop="description"><?php the_excerpt(); ?></div>

		<?php get_template_part('template-parts/social-media', 'post'); ?>

	</div>

</div>