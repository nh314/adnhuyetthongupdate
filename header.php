<?php

/**************************************

 * VUI LÒNG KHÔNG CHỈNH SỬA FILE NÀY. *

 **************************************

 * Chúng tôi không chịu trách nhiệm đối

 * vơí mọi vấn đề sảy ra cho website do

 * việc chỉnh sửa các tập tin mã nguồn

 * mà chưa có sự cho phép của chúng tôi.

 */

?>

<!doctype html>

<html class="no-js" <?php language_attributes(); ?> >

	<head>

		<meta charset="<?php bloginfo( 'charset' ); ?>" />

		<meta name="viewport" content="width=device-width, initial-scale=1.0" />

		<?php wp_head(); ?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-102967181-1', 'auto');
  ga('send', 'pageview');

</script>
<meta name="google-site-verification" content="CjlFJ7ND5Jf2F0lrG_vVKJ12Q4F7aOdaZD3GxRuzcIY" />

		<script>
			window.FontAwesomeConfig = {
  asyncEnabled: true,
  autoAccessibility: true,
  useUrl: "use.fontawesome.com",
  code: "40dbb647ad",
  webFontLoaderVersion: "1.6.24"
};
window.FontAwesome||(window.FontAwesome={}),function(){function a(a){this.el=a;for(var b=a.className.replace(/^\s+|\s+$/g,"").split(/\s+/),c=0;c<b.length;c++)d.call(this,b[c])}function b(a,b,c){Object.defineProperty?Object.defineProperty(a,b,{get:c}):a.__defineGetter__(b,c)}if(!("undefined"==typeof window.Element||"classList"in document.documentElement)){var c=Array.prototype,d=c.push,e=c.splice,f=c.join;a.prototype={add:function(a){this.contains(a)||(d.call(this,a),this.el.className=this.toString())},contains:function(a){return-1!=this.el.className.indexOf(a)},item:function(a){return this[a]||null},remove:function(a){if(this.contains(a)){for(var b=0;b<this.length&&this[b]!=a;b++);e.call(this,b,1),this.el.className=this.toString()}},toString:function(){return f.call(this," ")},toggle:function(a){return this.contains(a)?this.remove(a):this.add(a),this.contains(a)}},window.DOMTokenList=a,b(Element.prototype,"classList",function(){return new a(this)})}}(),function(a,b,c){function d(a){var c,d=[],e=b,f=e.documentElement.doScroll,g="DOMContentLoaded",h=(f?/^loaded|^c/:/^loaded|^i|^c/).test(e.readyState);h||e.addEventListener(g,c=function(){for(e.removeEventListener(g,c),h=1;c=d.shift();)c()}),h?setTimeout(a,0):d.push(a)}function e(){var a,c,d,e=b.querySelectorAll(".fa");Array.prototype.forEach.call(e,function(e){a=e.getAttribute("title"),e.setAttribute("aria-hidden","true"),c=e.nextElementSibling?!e.nextElementSibling.classList.contains("sr-only"):!0,a&&c&&(d=b.createElement("span"),d.innerHTML=a,d.classList.add("sr-only"),e.parentNode.insertBefore(d,e.nextSibling))})}function f(a){"undefined"!=typeof MutationObserver&&new MutationObserver(a).observe(b,{childList:!0,subtree:!0})}function g(){var a=b.createElement("link");a.href="https://"+l+"/"+m+".css",a.media="all",a.rel="stylesheet",b.getElementsByTagName("head")[0].appendChild(a)}function h(){var c=b.createElement("script"),d={};s=b.scripts[0],a.WebFontConfig||(a.WebFontConfig={}),d=a.WebFontConfig,d.custom||(d.custom={}),d.custom.families||(d.custom.families=[]),d.custom.urls||(d.custom.urls=[]),d.custom.families.push("FontAwesome"),d.custom.urls.push("https://"+l+"/"+m+".css"),c.src="https://"+l+"/webfontloader/"+n+"/webfontloader.js",s.parentNode.insertBefore(c,s)}function i(){return k&&(d(e),f(e)),j?h():g()}var j=a.FontAwesomeConfig.asyncEnabled,k=a.FontAwesomeConfig.autoAccessibility,l=a.FontAwesomeConfig.useUrl,m=a.FontAwesomeConfig.code,n=a.FontAwesomeConfig.webFontLoaderVersion;a.FontAwesome.load=i}(this,document);try{window.FontAwesome.load()}catch(e){}
		</script>
		

	</head>

	<body <?php body_class(); ?>   itemscope="" itemtype="http://schema.org/WebPage">

	<?php do_action( 'foundationpress_after_body' ); ?>



	<?php if ( get_theme_mod( 'wpt_mobile_menu_layout' ) == 'offcanvas' ) : ?>

	<div class="off-canvas-wrapper">

		<div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>

		<?php get_template_part( 'template-parts/mobile-off-canvas' ); ?>

	<?php endif; ?>



	<?php do_action( 'foundationpress_layout_start' ); ?>

	<div class="top-bar top-info">

		<div class="row">

			<div class="medium-3 columns hide-for-small-only"><?php bloginfo( 'description' ); ?></div>

			<div class="medium-9 columns text-center text-right-for-medium-up">
				<ul class="hotline-list">
					<?php foreach($GLOBALS['hotlines'] as $local => $number) :?>
					<li><a href="tel:<?=str_replace(array(' ', '.'), '', $number)?>" class="hotline"><span class="phone-icon"></span><?=$local?>: <strong><?=$number?></strong></a></li>
					<?php endforeach; ?>
				</ul>
			</div>

		</div>

	</div>

	

	<header id="masthead" class="site-header" role="banner">

		<div class="row expanded">

			<div class="large-5 columns">

				<a  itemprop="license" rel="license" class="logo" href="<?php echo esc_url( home_url( '/' ) ); ?>" ><img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo.svg" alt="Dịch vụ xét nghiệm ADN" title="Dịch vụ xét nghiệm ADN ở tại TpHCM" /></a>

				<span class="site-name" rel="home"><?php bloginfo( 'name' ); ?></span>
                <h1 id="title-h1" style="position:absolute; text-indent:-29999px;" <?=(($_SERVER['REQUEST_URI'] == '/')?' itemprop="name" ':'')?>><a class="site-name" href="<?php echo esc_url( home_url( '/' ) ); ?>" <?=(($_SERVER['REQUEST_URI'] == '/')?' itemprop="url" ':'')?>><strong><span>Dịch vụ phân tích ADN, Dịch vụ xét nghiệm ADN ở tại TpHCM</span></strong></a></h1>
                <p <?=(($_SERVER['REQUEST_URI'] == '/')?' itemprop="description" ':'')?> style="position:absolute; text-indent:-99999px;">Trung tâm dịch vụ xét nghiệm ADN ở tại TpHCM và Hà Nội chuyên nghiệp, tận tình, chu đáo, tin cậy, nhanh chóng và chính xác. Đây sẽ là nơi mà bạn luôn hài lòng với các dịch liên quan giám định ADN, phân tích ADN, dịch vụ kiểm tra ADN, dịch vụ xét nghiệm ADN và dịch vụ thử ADN.</p>
                <?=(($_SERVER['REQUEST_URI'] == '/')?' <p style="position:absolute; text-indent:-99999px; text-align:center"><img  itemprop="image" src="http://adnhuyetthong.com/uploads/ht/2016/08/hepatitis-c-chrontech-vaccine-dna-banner-01.jpg" alt="Dịch vụ phân tích ADN" /></p> ':'')?>

			</div>

			<div class="large-7 columns">

				<div class="title-bar" data-responsive-toggle="site-navigation">

					<button class="menu-icon" type="button" data-toggle="mobile-menu"></button>

					<div class="title-bar-title">

						<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>

					</div>

				</div>

				<nav id="site-navigation" class="main-navigation top-bar" role="navigation">			

					<div class="top-bar-right">

						<?php foundationpress_top_bar_r(); ?>



						<?php if ( ! get_theme_mod( 'wpt_mobile_menu_layout' ) || get_theme_mod( 'wpt_mobile_menu_layout' ) == 'topbar' ) : ?>

							<?php get_template_part( 'template-parts/mobile-top-bar' ); ?>

						<?php endif; ?>

					</div>

				</nav>

			</div>

		</div>

	</header>



	<section class="container">

		

		<?php do_action( 'foundationpress_after_header' );



