<div class="small-12 columns">

	<div class="content-title-only ">

		<h4 class="entry-title"><i class="fa fa-check text-primary"></i><a href="<?php echo esc_url( get_permalink() ) ?>" title="<?php the_title() ?>" rel="bookmark"><strong><span style="font-weight:normal"><?php the_title() ?></span></strong></a></h4>

		<?php get_template_part('template-parts/social-media', 'post'); ?>

		<?php

	echo '<time class="updated" datetime="' . get_the_time( 'c' ) . '">' . get_the_time('G:i l d-m-Y'). '</time>';

		 ?>

	<div class="clearfix"></div>

	</div>

</div>