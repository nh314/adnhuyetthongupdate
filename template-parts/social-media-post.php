
<div class="social-media-link-share">
	<a title="Chia sẻ lên Facebook" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode(esc_url( get_permalink() )) ?>" class="sm-facebook"><i class="fa fa-facebook"></i></a>
	<a title="Chia sẻ lên Twitter" target="_blank" href="https://twitter.com/home?status=<?php echo urlencode(esc_url( get_permalink() )) ?>" class="sm-twitter"><i class="fa fa-twitter"></i></a>
	<a title="Chia sẻ lên Google Plus" target="_blank" href="https://plus.google.com/share?url=<?php echo urlencode(esc_url( get_permalink() )) ?>" class="sm-google-plus"><i class="fa fa-google-plus"></i></a>	
</div>