<div class="social-media-link">
	<a href="<?php echo get_theme_mod('facebook_url'); ?>" target="_blank" class="sm-facebook"><i class="fa fa-facebook"></i></a>
	<a href="<?php echo get_theme_mod('twitter_url'); ?>" target="_blank" class="sm-twitter"><i class="fa fa-twitter"></i></a>
	<a href="<?php echo get_theme_mod('google_plus_url'); ?>" target="_blank" class="sm-google-plus"><i class="fa fa-google-plus"></i></a>
</div>