<?php
/**
 * Author: Ole Fredrik Lie
 * URL: http://olefredrik.com
 *
 * FoundationPress functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

/** Various clean up functions */
require_once( 'library/cleanup.php' );

/** Required for Foundation to work properly */
require_once( 'library/foundation.php' );

/** Register all navigation menus */
require_once( 'library/navigation.php' );

/** Add menu walkers for top-bar and off-canvas */
require_once( 'library/menu-walkers.php' );

/** Create widget areas in sidebar and footer */
require_once( 'library/widget-areas.php' );

/** Return entry meta information for posts */
require_once( 'library/entry-meta.php' );

/** Enqueue scripts */
require_once( 'library/enqueue-scripts.php' );

/** Add theme support */
require_once( 'library/theme-support.php' );

/** Add Nav Options to Customer */
require_once( 'library/custom-nav.php' );

/** Change WP's sticky post class */
require_once( 'library/sticky-posts.php' );

/** Configure responsive image sizes */
require_once( 'library/responsive-images.php' );

/** Configure customize options */
require_once( 'library/customize-options.php' );

/** If your site requires protocol relative url's for theme assets, uncomment the line below */
// require_once( 'library/protocol-relative-theme-assets.php' );


//Enqueue additional scripts
if ( ! function_exists( 'hthong_scripts' ) ) :
	function hthong_scripts(){
		//wp_enqueue_style( 'animated-css', get_template_directory_uri() . '/assets/javascript/vendor/wow/animate.css', array(), '', 'all' );
		wp_enqueue_style( 'owlcarousel-css', get_template_directory_uri() . '/assets/javascript/vendor/owl-carousel/owl.carousel.css', array(), '', 'all' );
		wp_enqueue_style( 'owlcarousel-theme', get_template_directory_uri() . '/assets/javascript/vendor/owl-carousel/owl.theme.css', array(), '', 'all' );
		wp_enqueue_script( 'owlcarousel-js', get_template_directory_uri() . '/assets/javascript/vendor/owl-carousel/owl.carousel.min.js', array('jquery'), '', true );		
		
		//wp_enqueue_script( 'wowjs', get_template_directory_uri() . '/assets/javascript/vendor/wow/wow.min.js', array(), '', true);
	}
add_action( 'wp_enqueue_scripts', 'hthong_scripts' );
endif;

if ( ! function_exists( 'hthong_excerpt_more' ) ) :
function hthong_excerpt_more() {
	$link = sprintf( '<a href="%1$s" class="more-link">%2$s</a>',
		esc_url( get_permalink( get_the_ID() ) ),
		/* translators: %s: Name of current post */
		sprintf( __( '[đọc tiếp]', 'hthong' ), get_the_title( get_the_ID() ) )
	);
	//return ' &hellip; ' . $link;
	return '&hellip;';
}
add_filter( 'excerpt_more', 'hthong_excerpt_more' );
endif;
if ( ! function_exists( 'hthong_excerpt_length' ) ) :
function hthong_excerpt_length( $length ) {
	return 38;
}
add_filter( 'excerpt_length', 'hthong_excerpt_length', 999);
endif;

add_image_size('service-thumb', 360, 313, true);
add_image_size('vertical-thumb', 310, 330, true);
//Template Tags
function hthong_post_thumbnail($thumb_style = null) {
	if ( post_password_required() || is_attachment() || ! has_post_thumbnail() ) {
		return;
	}

	if ( is_singular() ) :
	?>

	<div class="post-thumbnail">
		<?php the_post_thumbnail($thumb_style); ?>
	</div><!-- .post-thumbnail -->

	<?php else : ?>

	<a class="post-thumbnail" href="<?php the_permalink(); ?>" aria-hidden="true">
		<?php the_post_thumbnail( $thumb_style, array( 'alt' => the_title_attribute( 'echo=0' ) ) ); ?>
	</a>

	<?php endif; // End is_singular()
}
//-----------------------------------

if ( ! function_exists( 'hthong_feedback_merge_title' ) ) :
	function hthong_feedback_merge_title($content){
		if(in_category(get_theme_mod('phanhoi_cat'))){
			$content = wp_strip_all_tags($content). ' - <b>'. get_the_title() .'</b>';
		}
		return $content;
	}
endif;
add_filter( 'the_content', 'hthong_feedback_merge_title' ); 


if (!function_exists('hthong_prepare_hotline')):
	function hthong_prepare_hotline()
	{
		$hotlinePage = get_page_by_path('hotline');
		
		if (isset($hotlinePage)) {
			preg_match_all('/(?<locat>.*)\:(?<number>.*)/', wp_strip_all_tags($hotlinePage->post_content), $result);
			$hotlines = array_combine(array_map('trim', $result['locat']), array_map('trim', $result['number']));
			
		} else {
			
			//Default page content
			$hotlines = array(
				'City Name 1' => 'Phone number 1',
				'City Name 2' => 'Phone number 2',
			);
			
			$page_content = array_reduce(
				array_map(function($locat, $number) {
					return $locat . ': ' . $number;
				}, array_keys($hotlines), $hotlines),
				function($c, $i){
				return $c.= $i . "\n";
			});
			
			$pageArr = array(
				'post_title' => 'Hotline',
				'post_content' => $page_content,
				'post_name' => 'hotline',
				'post_status' => 'publish',
				'post_type'	=> 'page'
			);
			wp_insert_post($pageArr, true);
			
		}
		
		return $hotlines;
		
		
	}
endif;

if (!function_exists('hthong_get_default_hotline')):
	function hthong_get_default_hotline()
	{
		global $hotlines;
		foreach ($hotlines as $locat => $number) {
			if (strlen($number) > 0) return $number;
		}
	}
	
endif;

$hotlines = hthong_prepare_hotline();


function hthong_phone_icon_ringing_shortcode($atts, $content = null){
	$a = shortcode_atts( array(
        'number' => '0912345678',
    ), $atts );
    if (strlen($content) > 0) {
    	$html = "<a href=\"tel:{$a['number']}\"><span class=\"ringing-phone-icon\"></span>$content</a>";
    } else {
    	$html ="<span class=\"ringing-phone-icon\"></span>";
    }
	return $html;
}
add_shortcode( 'phone-icon', 'hthong_phone_icon_ringing_shortcode' );
