<div class="large-6 columns content-2-columns">

	<div class="row">

		<div class="medium-6 columns medium-push-6">

			<div class="thumb-overlay"><?php hthong_post_thumbnail('vertical-thumb') ?></div>

		</div>

		<div class="medium-6 columns medium-pull-6">

			<?php the_title( sprintf( '<h3 class="entry-title"><a href="%s" rel="bookmark"  itemprop="url"><span itemprop="itemListElement">', esc_url( get_permalink() ) ), '</span></a></h3>' ); ?>

			<div class="entry-content">

				<div itemprop="description"><?php the_excerpt(); ?></div>

				<?php get_template_part('template-parts/social-media', 'post'); ?>

			</div>



		</div>

		

	</div>

</div>