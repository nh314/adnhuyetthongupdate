<?php

/**************************************

 * VUI LÒNG KHÔNG CHỈNH SỬA FILE NÀY. *

 **************************************

 * Chúng tôi không chịu trách nhiệm đối

 * vơí mọi vấn đề sảy ra cho website do

 * việc chỉnh sửa các tập tin mã nguồn

 * mà chưa có sự cho phép của chúng tôi.

 */

get_header(); ?>
<?php if (function_exists('putRevSlider')): ?>
<?php putRevSlider( 'mainslider' ); ?>
<?php endif; ?>
<section class="bg-primary">

	<div class="row">

		<div class="medium-4 columns">

			<a href="<?php echo get_theme_mod('noibat_1_lienket'); ?>" class="feature-index wow slideInLeft">

				<i class="ft-icon ft-exact"></i>		

				<div class="ft-caption"><?php echo get_theme_mod('noibat_1_tieude'); ?></div>

				<div class="ft-content">

					<?php echo get_theme_mod('noibat_1_noidung'); ?>

				</div>			

			</a>

		</div>

		<div class="medium-4 columns">

			<a href="<?php echo get_theme_mod('noibat_2_lienket'); ?>" class="feature-index wow slideInUp">

				<i class="ft-icon ft-private"></i>

				<div class="ft-caption"><?php echo get_theme_mod('noibat_2_tieude'); ?></div>

				<div class="ft-content">

					<?php echo get_theme_mod('noibat_2_noidung'); ?>

				</div>

			</a>

		</div>

		<div class="medium-4 columns">

			<a href="<?php echo get_theme_mod('noibat_3_lienket'); ?>" class="feature-index wow slideInRight">

				<i class="ft-icon ft-easy"></i>

				<div class="ft-caption"><?php echo get_theme_mod('noibat_3_tieude'); ?></div>

				<div class="ft-content">

					<?php echo get_theme_mod('noibat_3_noidung'); ?>

				</div>

			</a>

		</div>

	</div>

</section>

<section class="index-row-1 row column wow slideInLeft">
	<?php global $post; // required

	$args = array('category' => get_theme_mod('dichvu_cat'), 'numberposts' => -1, 'order' => 'ASC', 'post_status' => 'publish');

	$dich_vu = get_posts($args);



	?>
	<?php dynamic_sidebar( 'index-row-1' ); ?>
  <div id="dichvu" itemscope="" itemtype="http://schema.org/ItemList">
    <div class="row column">
		<div class="column">
          <h2 class="section-title2"><span itemprop="name"><strong>Dịch vụ xét nghiệm giám định ADN</strong></span></h2>
          <link itemprop="itemListOrder" href="http://schema.org/ItemListOrderDescending" />
        </div>
	</div>
	<div class="dich-vu">
		<?php
		foreach($dich_vu as $post) : setup_postdata($post);
			get_template_part('template-parts/content', 'excerpt');
		endforeach;
		?>
	</div>
  </div>  
</section>

<section class="index-row-2 wow slideInRight">

	<?php global $post; // required

	$args = array('category' => get_theme_mod('hotro_cat')); // exclude category 9

	$bai_viet_ho_tro = get_posts($args);



	//var_dump($bai_viet_ho_tro);?>

	<div class="ho-tro" id="hotro"  itemscope="" itemtype="http://schema.org/ItemList">

		<div class="row">

		<div class="column">
          <h2 class="section-title2"><strong><span itemprop="name">Hỗ trợ tư vấn khách hàng phân tích xét nghiệm ADN</span></strong></h2>
          <link itemprop="itemListOrder" href="http://schema.org/ItemListOrderDescending" />
        </div>

		<?php

		foreach($bai_viet_ho_tro as $post) : setup_postdata($post);



			get_template_part('template-parts/content', '2-columns');

		endforeach;

		?>

		</div>

	</div>

	<?php dynamic_sidebar( 'index-row-2' ); ?>

</section>



<section class="index-row-1">

	<div class="row">

		<div class="large-6 columns wow fadeInLeft">

			<?php dynamic_sidebar( 'video-sidebar-left' ); ?>

			

		</div>

		<div class="large-6 columns wow fadeInRight">

			<?php dynamic_sidebar( 'video-sidebar' ); ?>

		</div>

	</div>

</section>



<section class="index-row-2">	

	<div class="row">
		<div class="large-6 columns wow fadeInLeft" id="newshome" itemscope="" itemtype="http://schema.org/ItemList">
			<h2 class="section-title2"><?php echo sprintf("<a href='%s'><span itemprop=\"name\">%s</span></a>", get_category_link(get_theme_mod('tintuc_cat')), get_cat_name(get_theme_mod('tintuc_cat'))); ?></h2>
            <link itemprop="itemListOrder" href="http://schema.org/ItemListOrderDescending" />
			<?php global $post; // required
				$args = array('category' => get_theme_mod('tintuc_cat'), 'posts_per_page' => 5);
				$tintuc = get_posts($args);?>
			<div class="row">
				<?php
				$index = 0;
				foreach($tintuc as $post) : setup_postdata($post);
					if($index<2){
						get_template_part('template-parts/content', '2-columns-hr');	
					}else{
						get_template_part('template-parts/content', 'title-only');
					}
					$index++;
				endforeach;
				?>
			</div>
			<h4 class="widget-title phanhoi-title"><i class="fa fa-user"></i> <?php echo get_cat_name(get_theme_mod('phanhoi_cat')) ?></h4>
			<div class="phan-hoi">
				<?php $fb_query = new WP_Query(array('cat' => get_theme_mod('phanhoi_cat'))); ?>
				
				<?php while ($fb_query->have_posts()) : $fb_query->the_post(); 
					remove_filter( 'the_content', 'A2A_SHARE_SAVE_add_to_content', 98 ); ?>

				<div>
					<div class="content">
						<?php the_content(); ?>									
					</div>
				</div>

				<?php endwhile; ?>

			</div>
		</div>
		

		<div class="large-6 columns wow fadeInRight">

			<?php

				$query = new WP_Query(array( 'cat' => get_theme_mod('faq_cat') ));

			?>

			<h2 class="section-title2"><?php echo sprintf("<a href='%s'>%s</a>", get_category_link(get_theme_mod('faq_cat')), get_cat_name(get_theme_mod('faq_cat'))); ?></h2>

			<ul class="accordion faq-accordion" data-accordion data-allow-all-closed="true">

			<?php while ($query->have_posts()): $query->the_post();?>

				<?php remove_filter( 'the_content', 'A2A_SHARE_SAVE_add_to_content', 98 );?>

				<li class="accordion-item" data-accordion-item>

					<a href="#" class="accordion-title"><?php the_title();?></a>

					<div class="accordion-content" data-tab-content>

						<?php



						the_content(); ?>

					</div>

				</li>

			<?php endwhile; ?>

			</ul>

		</div>



	</div>
    <div vocab="http://schema.org/" typeof="Blog" style="text-align:center; width:100%">    
      <div property="name" class="kksr-title">Dịch vụ xét nghiệm ADN</div>     
      <div property="aggregateRating" typeof="AggregateRating">
          <span property="ratingValue">5.0</span> / 5 với 
          <span property="ratingCount">6352</span> bình chọn            
          <meta property="bestRating" content="5"/>            
          <meta property="worstRating" content="1"/>    
      </div>
      <div style="clear:left"></div>
    </div>

</section>



<?php get_footer();

