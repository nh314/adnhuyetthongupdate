<?php
/**
 * Register widget areas
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

if ( ! function_exists( 'foundationpress_sidebar_widgets' ) ) :
function foundationpress_sidebar_widgets() {
	register_sidebar(array(
	  'id' => 'sidebar-widgets',
	  'name' => __( 'Sidebar widgets', 'foundationpress' ),
	  'description' => __( 'Drag widgets to this sidebar container.', 'foundationpress' ),
	  'before_widget' => '<article id="%1$s" class="widget %2$s">',
	  'after_widget' => '</article>',
	  'before_title' => '<h6 class="widget-title">',
	  'after_title' => '</h6>',
	));	
}

add_action( 'widgets_init', 'foundationpress_sidebar_widgets' );
endif;

/**
 * Register widget areas
 */
if ( ! function_exists( 'hthong_sidebar_widgets' ) ) :
function hthong_sidebar_widgets() {
	$sidebars = array(
		array(
			'id' => 'index-row-1',
			'name' => __( 'Trang chủ vùng 1', 'hthong' ),
			'description' => __( 'Nội dung hiện thị vùng 1 trang chủ', 'hthong' ),
			'before_widget' => '<div class="row column"><section id="%1$s" class="%2$s column">',
			'after_widget' => '</section></div>',
			'before_title' => '<h6 class="section-title2">',
			'after_title' => '</h6>',
			),
		array(
			'id' => 'index-row-2',
			'name' => __( 'Trang chủ vùng 2', 'hthong' ),
			'description' => __( 'Nội dung hiện thị vùng 2 trang chủ', 'hthong' ),
			'before_widget' => '<section id="%1$s" class="%2$s">',
			'after_widget' => '</section>',
			'before_title' => '<h6 class="section-title2">',
			'after_title' => '</h6>',
			),
		array(
			'id' => 'video-sidebar-left',
			'name' => __( 'Bên trái Video', 'hthong' ),			
			'before_widget' => '<section id="%1$s" class="%2$s">',
			'after_widget' => '</section>',
			'before_title' => '<h6 class="section-title2">',
			'after_title' => '</h6>',
			),
		array(
			'id' => 'video-sidebar',
			'name' => __( 'Video', 'hthong' ),			
			'before_widget' => '<section id="%1$s" class="%2$s">',
			'after_widget' => '</section>',
			'before_title' => '<h6 class="section-title2">',
			'after_title' => '</h6>',
			),

		array(
			'id' => 'footer-sidebar',
			'name' => __( 'Phần chân', 'hthong' ),
			'description' => __( 'Nội dung phần chân', 'hthong' ),
			'before_widget' => '<section id="%1$s" class="%2$s">',
			'after_widget' => '</section>',
			'before_title' => '<h6 class="widget-title">',
			'after_title' => '</h6>',
			),
		array(
			'id' => 'footer-sidebar2',
			'name' => __( 'Phần chân 2', 'hthong' ),
			'description' => __( 'Nội dung phần chân 2', 'hthong' ),
			'before_widget' => '<section id="%1$s" class="%2$s">',
			'after_widget' => '</section>',
			'before_title' => '<h6 class="widget-title">',
			'after_title' => '</h6>',
			),
		array(
			'id' => 'footer-sidebar3',
			'name' => __( 'Phần chân 3', 'hthong' ),
			'description' => __( 'Nội dung phần chân 3', 'hthong' ),
			'before_widget' => '<section id="%1$s" class="%2$s">',
			'after_widget' => '</section>',
			'before_title' => '<h6 class="widget-title">',
			'after_title' => '</h6>',
			),
		array(
			'id' => 'footer-sidebar4',
			'name' => __( 'Phần chân 4', 'hthong' ),
			'description' => __( 'Nội dung phần chân 4', 'hthong' ),
			'before_widget' => '<section id="%1$s" class="%2$s">',
			'after_widget' => '</section>',
			'before_title' => '<h6 class="widget-title">',
			'after_title' => '</h6>',
			),
	);
	foreach ($sidebars as $sidebar) {
		register_sidebar($sidebar);
	}
}
add_action( 'widgets_init', 'hthong_sidebar_widgets' );
endif;